import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@javax.servlet.annotation.WebServlet(name = "CottageBookingServlet", urlPatterns = "/servlet")
public class CottageBookingServlet extends javax.servlet.http.HttpServlet {

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        String name = request.getParameter("name");
        if (name == null) name = "Anonymous";

        InputStream
        String places = request.getParameter("requiredAmountOfPlaces");
        int amountOfPlaces = (places == null) ? Integer.parseInt(places) : 1;

        String bedrooms = request.getParameter("requiredAmountOFBedrooms");
        int amountOfBedrooms = (bedrooms == null) ? Integer.parseInt(bedrooms) : 1;

        String lakeDistance = request.getParameter("maxDistanceFromLake");
        int distanceFromLake = (lakeDistance == null) ? Integer.parseInt(lakeDistance) : 1000;

        String cityDistance = request.getParameter("maxDistanceFromCity");
        int distanceToCity = (lakeDistance == null) ? Integer.parseInt(cityDistance) : 100;

        String city = request.getParameter("nearestCity");
        if (city == null) city = "";

        String days = request.getParameter("amountOfDays");
        int amountOfDays = (lakeDistance == null) ? Integer.parseInt(cityDistance) : 1;

        String date = request.getParameter("startingDate");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startingDate;
        if (date == null) startingDate = new Date();
            else try {
            startingDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String shiftStr = request.getParameter("shift");
        int shift = (shiftStr == null) ? Integer.parseInt(shiftStr) : 0;


        response.setContentType("application/xml");
        response.setStatus(200);

        OntModel ontModel = ModelFactory.createOntologyModel();
        ontModel.read(new String());
        Dataset dataset = DatasetFactory.create(ontModel);
        String queryString = "PREFIX : <http://example.com/#> " +
                "PREFIX cbo:   <http://localhost:8080/task1/CottageBookingOntology#> " +
                "SELECT ?name " +
                "FROM FROM <http://localhost:8080/task1/cottagesDatabase.ttl> " +
                "WHERE { ?Response cbo:name ?name }";

        String queryStr = "PREFIX : <http://example.com/#> "+
                "PREFIX cbo:   <http://localhost:8080/task1/CottageBookingOntology.ttl> "+
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
                "CONSTRUCT { :response cbo:hasOffer ?offer; "+
                "cbo:name \""+name+"\". "+
                "?offer cbo:period :period. "+
                ":period rdf:type cbo:period; "+
                "cbo:startingDate \"2015-04-01\"; "+
                "cbo:endingDate \"2015-04-08\". "+
                "?cottage rdf:type cbo:Cottage; "+
                "cbo:address ?address; "+
                "cbo:imageURL ?imageURL;"+
                "cbo:amountOfPlaces ?amountOfPlaces; "+
                "cbo:amountOfBedrooms ?amountOfBedrooms; "+
                "cbo:distanceFromLake ?distanceFromLake; "+
                "cbo:distanceToCity ?distanceToCity; "+
                "cbo:nearestCity ?nearestCity.} "+
                "FROM <http://localhost:8080/task1/cottagesDatabase.ttl> "+
                "WHERE { ?offer rdf:type cbo:Offer; "+
                "cbo:hasCottage ?cottage. "+
                "?cottage cbo:address ?address; "+
                "cbo:imageURL ?imageURL; "+
                "cbo:amountOfPlaces ?amountOfPlaces; "+
                "cbo:amountOfBedrooms ?amountOfBedrooms; "+
                "cbo:distanceFromLake ?distanceFromLake; "+
                "cbo:distanceToCity ?distanceToCity; "+
                "cbo:nearestCity ?nearestCity."+
                "FILTER (?amountOfPlaces >= "+amountOfPlaces+"). "+
                "FILTER (?amountOfBedrooms >= "+amountOfBedrooms+"). "+
                "FILTER (?distanceFromLake <= "+distanceFromLake+"). "+
                "FILTER (?distanceToCity <= "+distanceToCity+"). }";
        Query query = QueryFactory.create(queryString);
        QueryExecution queryExecution = QueryExecutionFactory.create(query);
        ResultSet resultSet = queryExecution.execSelect();
        while(resultSet.hasNext()) {
            QuerySolution qs = resultSet.nextSolution();
            System.out.println(qs);
        }
        Model model = queryExecution.execConstruct();
        queryExecution.close();

        model.write(response.getOutputStream());

    }
}