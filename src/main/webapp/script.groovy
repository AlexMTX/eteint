@Grab('org.apache.jena:jena-core:2.13.0')
@Grab('org.apache.jena:jena-arq:2.13.0')


import java.io.InputStream;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Model;

/*
String queryString =	"PREFIX : <http://example.com/#> "+
						"PREFIX cbo:   <http://localhost:8080/task1/CottageBookingOntology.ttl> "+
						"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
						"CONSTRUCT { :response cbo:hasOffer ?offer. "+
						"?offer cbo:period :period. "+
						":period rdf:type cbo:period; "+
						"cbo:startingDate \"2015-04-01\"; "+
						"cbo:endingDate \"2015-04-08\". "+
						"?cottage rdf:type cbo:Cottage; "+
						"cbo:address ?address; "+
						"cbo:imageURL ?imageURL;"+
						"cbo:amountOfPlaces ?amountOfPlaces; "+
						"cbo:amountOfBedrooms ?amountOfBedrooms; "+
						"cbo:distanceFromLake ?distanceFromLake; "+
						"cbo:distanceToCity ?distanceToCity; "+
						"cbo:nearestCity ?nearestCity.} "+
						"FROM <file:cottagesDatabase.ttl> "+
						"WHERE { ?offer rdf:type cbo:Offer; "+
						"cbo:hasCottage ?cottage. "+
						"?cottage cbo:address ?address; "+
						"cbo:imageURL ?imageURL; "+
						"cbo:amountOfPlaces ?amountOfPlaces; "+
						"cbo:amountOfBedrooms ?amountOfBedrooms; "+
						"cbo:distanceFromLake ?distanceFromLake; "+
						"cbo:distanceToCity ?distanceToCity; "+
						"cbo:nearestCity ?nearestCity."+
						"FILTER (?amountOfPlaces >= 3). "+
						"FILTER (?amountOfBedrooms >= 1). "+
						"FILTER (?distanceFromLake <= 500). "+
						"FILTER (?distanceToCity <= 100). }"
*/

Model model = ModelFactory.createDefaultModel();
InputStream in = FileManager.get().open( "cottagesDatabase.ttl" );
if (in == null) {
  throw new IllegalArgumentException(
  "File: " + "cottagesDatabase.ttl" + " not found");
}

// read the RDF/XML file
model.read(in, "TURTLE");
 Dataset dataset = DatasetFactory.create(model);
String queryString = "PREFIX : <http://example.com/#> "+
						"PREFIX cbo:   <http://localhost:8080/task1/CottageBookingOntology.ttl> "+
						"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
						"SELECT ?name "+
				//		"FROM <file:cottagesDatabase.ttl> "+
						"WHERE { ?response cbo:name ?name }";

Query query = QueryFactory.create(queryString);
QueryExecution qe = QueryExecutionFactory.create(query,dataset);
ResultSet resultSet = qe.execSelect();
println(resultSet.hasNext())
while(resultSet.hasNext()) {
            QuerySolution qs = resultSet.nextSolution();
            println(qs);
        }
